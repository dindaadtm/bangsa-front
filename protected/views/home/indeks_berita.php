<section class="indeks-berita">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-8">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">INDEKS BERITA</li>
                    </ol>
                </nav>
                <div class="clear height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>/370x125.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>/370x125.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clear-height-20"></div>
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>INDEKS BERITA</p>
                            </div>
                            <div class="clear-height-20"></div>
                            <div class="tampilkan">
                                <div class="col-md-6">
                                    <div class="col-md-7">
                                    <p class="teks-view">Tampilkan Berdasarkan Tanggal : </p>

                                    </div>
                                    <div class="form-group col-md-5">
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option label="Pilih Tanggal"></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-7">
                                    <p class="teks-view">Tampilkan Berdasarkan Kanal : </p>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option label="Pilih Kanal"></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clear-height-20"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="box col-md-12">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title">
                                <p>JATIM</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                        <?php for ($i = 1; $i <= 3; $i++) {?>
                            <div class="row">

                                <div class="clear-width-20">
                                    <div class="col-md-12">
                                        <a href="">
                                        <img class="image2 img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                        <div class="date"><p>18 April 2019</p></div>
                                        <div class="title"><p>Overseas Adventure Travel In Nepal</p></div>
                                        </a>                                    
                                    </div>
                                </div>
                            </div>
                        <div class="clear-height-20"></div>

                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>

                <div class="row">
                    <div class="box col-md-12">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title">
                                <p>NASIONAL</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                        <?php for ($i = 1; $i <= 3; $i++) {?>
                            <div class="row">

                                <div class="clear-width-20">
                                    <div class="col-md-12">
                                        <a href="">
                                        <img class="image2 img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                        <div class="date"><p>18 April 2019</p></div>
                                        <div class="title"><p>Overseas Adventure Travel In Nepal</p></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <div class="clear-height-20"></div>

                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>

                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-green">
                            Lebih Banyak
                        </button>
                    </div>
                </div>
                <div class="clear-height-20"></div>

                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php echo $this->renderPartial('//layouts/right-templates-no-opini', array()); ?>
            </div>
                <div class="clear"></div>
        </div>
</section>