<section class="search-sec">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-8">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">INDEKS BERITA</li>
                    </ol>
                </nav>
                <div class="clear height-20"></div>

                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clear-height-5"></div>
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>Hasil Pencarian : "SURABAYA"</p>
                            </div>
                        </div>
                        <div class="col-md-12 hasil-pencarian">
                            <div class="result col-md-6">
                                <p>Sekitar 250 Hasil Pencarian</p>
                            </div>
                            <div class="sortir col-md-6">
                                <div class="col-md-6">
                                    <p class="sortir-text">Sortir Menurut </p>
                                </div>
                                <div class="form-group col-md-6">
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>
                <div class="content-search">
                    <?php for ($i = 1; $i <= 5; $i++) {?>
                        <div class="post">
                            <div class="row">

                                <div class="clear-width-20">
                                    
                                    <div class="col-md-12">
                                        <div class="date"><p>18 April 2019</p></div>
                                        <a href=""><div class="title"><p>Overseas Adventure Travel In Nepal Overseas Adventure Travel In Nepal</p></div></a>
                                    </div>
                                    <div class="col-md-12">
                                        <img class="image2 img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                        <div class="desc"><p>Overseas Adventure Travel In NepalOverseas Adventure Travel In NepalOverseas Adventure Travel In NepalOverseas Adventure Travel In Nepal</p></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="clear-height-20"></div>

                        <?php } ?>
                </div>

                <div class="col-md-12">
                            <div class="pagination">
                                <a class="active" href="">Sebelumnya</a>
                                <a class="active" href="">1</a>
                                <a href="">2</a>
                                <a href="">3</a>
                                <a class="active" href="">Berikutnya</a>
                            </div>
                        </div>
                <div class="clear-height-20"></div>

                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <?php echo $this->renderPartial('//layouts/right-templates-no-opini', array()); ?>
            </div>
                <div class="clear"></div>
        </div>
</section>