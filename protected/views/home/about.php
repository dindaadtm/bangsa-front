<section class="about-sec-1">
    <div class="container">
        <div class="clear clearfix height-15"></div>
        <div class="row">
            <nav class="nav-bread" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tentang Kami</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="clear-height-30"></div>
                <div class="title-tentang-kami">
                    Tentang Kami
                </div>
                <div class="clear-height-20"></div>
                <img src="<?php echo $this->assetBaseurl; ?>linehorizontal.jpg">
                <div class="clear-height-30"></div>
                <div class="name-company">PT. BANGSA SEJAHTERA PERS</div>
                <div class="clear-height-20"></div>
                <div class="web-address">Media Online bangsaonline.com</div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Direktur / Penanggung Jawab :
                    </div>
                    <div class="name-owner-2">
                        HM Mas'ud Adnan
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Pemimpin Perusahaan :
                    </div>
                    <div class="name-owner-2">
                        M Dikman
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Pemimpin Redaksi :
                    </div>
                    <div class="name-owner-2">
                        Revol Afkar
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Redaktur Pelaksana :
                    </div>
                    <div class="name-owner-2">
                        Maulana
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Kordinator Liputan :
                    </div>
                    <div class="name-owner-2">
                        Rosihan Choirul Anwar
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Redaktur :
                    </div>
                    <div class="name-owner-2">
                        Revol Afkar,Abdurrahman Ubaidah,
                        <br/>Nur Syaifuddin,Rosihan Choirul Anwar
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Iklan dan Pemasaran :
                    </div>
                    <div class="name-owner-2">
                        Yuni,Kemal Rizal
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Desain & Teknologi Informasi :
                    </div>
                    <div class="name-owner-2">
                        M Dikman (kepala), Haksni, Aulia Rahman
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        keuangan :
                    </div>
                    <div class="name-owner-2">
                        Novi, Tutut
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Sekretaris Redaksi / Umum :
                    </div>
                    <div class="name-owner-2">
                        Yayuk
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="clear-height-30">
                    <div class="label-owner">
                        Bojonegoro :
                    </div>
                    <div class="name-owner">
                        Eky Nurhadi
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Sampang :
                    </div>
                    <div class="name-owner">
                        Ahmad Bahri
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Pamekasan :
                    </div>
                    <div class="name-owner">
                        Erri Sugianto
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Sumenep :
                    </div>
                    <div class="name-owner">
                        Faisal
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Kab. Pasuruan :
                    </div>
                    <div class="name-owner">
                        Supardi, Ahmad Habibi
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Kota Pasuruan :
                    </div>
                    <div class="name-owner">
                        Ahmad Fuad
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Bondowoso :
                    </div>
                    <div class="name-owner">
                        Sugiyanto
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Nganjuk :
                    </div>
                    <div class="name-owner">
                        Bambang DJ
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Ngawi :
                    </div>
                    <div class="name-owner">
                        Zainal Abidin
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="clear-height-30">
                    <div class="label-owner">
                        Reporter Surabaya :
                    </div>
                    <div class="name-owner">
                        Nur Khasanah Yulistiani,
                        <br/>M Didi Rosadi, Irwan Susanto,Devi Afriyanti,
                        <br/>Yudi Arianto
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Sidoarjo :
                    </div>
                    <div class="name-owner">
                        Musta'in, Catur A Erlambang
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Mojokerto :
                    </div>
                    <div class="name-owner">
                        Rohmad Saiful Aris,
                        <br/> Yudi Eko Purnomo
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Jombang :
                    </div>
                    <div class="name-owner">
                        Adi Susanto, Rony Suhartomo,
                        <br/> Romza
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Gresik :
                    </div>
                    <div class="name-owner">
                        M Syuhud Almanfaluty
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Tuban :
                    </div>
                    <div class="name-owner">
                        Suwardi
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Trenggalek :
                    </div>
                    <div class="name-owner">
                        Herman Subagyo
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Kediri :
                    </div>
                    <div class="name-owner">
                        Arif Kurniawan
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="clear-height-30">
                    <div class="label-owner">
                        Situbondo :
                    </div>
                    <div class="name-owner">
                        Hadi Prayitno
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Jember :
                    </div>
                    <div class="name-owner">
                        Yudi Indrawan
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Probolinggo :
                    </div>
                    <div class="name-owner">
                        Andi Sirajuddin
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Lumajang :
                    </div>
                    <div class="name-owner">
                        Imron Ghozali
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Blitar :
                    </div>
                    <div class="name-owner">
                        Tri Susanto
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Malang Raya :
                    </div>
                    <div class="name-owner">
                        Tuhu Prayono,
                        <br/> Iwan Setiawan,
                        <br/> Anik
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Tulungagung :
                    </div>
                    <div class="name-owner">
                        Feri Wahyudi
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Madiun :
                    </div>
                    <div class="name-owner">
                        Hendro,Zainal Abidin
                    </div>
                </div>
                <div class="clear-height-30">
                    <div class="label-owner">
                        Ponorogo :
                    </div>
                    <div class="name-owner">
                        Yahya
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="clear-height-30">
                    <div class="online">
                        Online Sejak Juli 2014
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>