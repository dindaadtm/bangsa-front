<section class="kontak">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-12">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kontak Kami</li>
                    </ol>
                </nav>
                <div class="detail-blog-content">

                    <div class="clear-height-5"></div>
                    <div class="title-news-2">
                        <p>Kontak Kami</p>
                    </div>
                    <div class="line">
                    <img src="<?php echo $this->assetBaseurl; ?>linehorizontal.jpg" alt=""></div>
                    <div class="pt">
                        <p class="atas">PT. BANGSA SEJAHTERA PERS</p>
                        <p class="bawah">Media Online Bangsaonline.com</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="kontak">
                                <p class="title">Alamat Kantor :</p>
                                <p>Jalan Gayung Sari IX / 5 Surabaya </p>
                            </div>
                            <div class="clear-height-20"></div>

                            <div class="kontak">
                                <p class="title">Telepon :</p>
                                <p>(031) 827 16 24
                                    <br>(031) 827 09 61
                                    <br>(031) 827 16 67 </p>
                            </div>
                            <div class="clear-height-20"></div>

                            <div class="kontak">
                                <p class="title">Faks :</p>
                                <p>(031) 827 09 67 </p>
                            </div>
                            <div class="clear-height-20"></div>

                            <div class="kontak">
                                <p class="title">Email :</p>
                                <p>bangsaonline@gmail.com </p>
                            </div>
                            <div class="clear-height-20"></div>

                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>maps.png" class="img-responsive">
                        </div>
                    </div>

                </div>
            </div>
            <div class="clear"></div>
        </div>
</section>