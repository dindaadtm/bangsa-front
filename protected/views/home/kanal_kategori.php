<section class="kanal-kategori">
    <div class="prelative container">
        <div class="clear clearfix height-15"></div>
        <div class="row">
            <div class="col-md-8 ">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">JATIM</li>
                    </ol>
                </nav>
                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>JATIM</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="clear-height-10"></div>
                        <div class="col-md-12">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>newspage.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-12">
                            <div class="title-artikel">
                            <div class="clear-height-20"></div>

                                Travel Health Useful Medical Information For Good Health Be
                            </div>
                            <div class="clear-height-10"></div>

                            <div class="tanggal-image">
                                <p>08 April 2019</p>
                            </div>
                            <div class="clear-height-20"></div>

                        </div>
                    </div>
                </div>

                <div class="row">
                        <div class="box col-md-12">
                            <div class="box-title">
                            <div class="clear-height-8"></div>
                                <div class="title" >
                                    <p>Terkini</p>
                                </div>
                                <div class="clear-height-8"></div>
                            </div>
                            <div class="box-content">
                                <div class="clear-width-20">
                                    <div class="row">
                                    <?php for ($i = 1; $i <= 6; $i++) {?>
                                        <div class="col-md-4">
                                            <a href="">
                                                <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                                <div class="clear-height-15"></div>
                                                <div class="title">
                                                    <p>A Discount Toner Cartridge Is Better Then Ever And You Will</p>
                                                </div>
                                                <div class="clear-height-5"></div>
                                                <div class="date"><p>01 Oct 2018</p>
                                                </div>
                                                <div class="clear-height-15"></div>
                                                <div class="description"><p>A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC.These handy little</p></div>
                                                <div class="clear-height-15"></div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pagination">
                                <a class="active" href="">Sebelumnya</a>
                                <a class="active" href="">1</a>
                                <a href="">2</a>
                                <a href="">3</a>
                                <a class="active" href="">Berikutnya</a>
                            </div>
                        </div>
                  
                </div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php echo $this->renderPartial('//layouts/right-templates-no-opini', array()); ?>
            </div>
        </div>
    </div>
</section>