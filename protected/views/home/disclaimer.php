<section class="disclaimer">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-12">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Disclaimer</li>
                    </ol>
                </nav>
                <div class="detail-blog-content">

                    <div class="clear-height-5"></div>
                    <div class="title-news-2">
                        <p>Disclaimer</p>
                    </div>
                    <div class="line">
                    <img src="<?php echo $this->assetBaseurl; ?>linehorizontal.jpg" alt=""></div>
                    <div class="isi">
                        <p>Dengan mengakses dan menggunakan bangsaonline.com, berarti anda memahami dan setuju, bahwa : <br><br>
                        Berita, foto dan video yang dimuat di bangsaonline.com hanya sebagai informasi dan tidak diharapkan untuk tujuan transaksi lain.<br><br> Bangsaonline.com berusaha memuat berita di semua konten seakurat mungkin, namun bangsaonline dan semua mitra penyedia informasi, dan pihak lain di situs ini, tidak bertanggungjawab atas segala kesalahan dan keterlambatan memperbarui data atau informasi, atau segala kerugian yang ditimbulkan karena tindakan berkaitan penggunaan informasi yang disajikan. <br><br>Bangsaonline.com tak bertanggungjawab atas akibat langsung ataupun tidak langsung dari semua teks, gambar, suara, video dan segala bentuk grafis yang dihasilkan dan disampaikan pembaca atau pengguna di rubrik publik seperti Teenage, Komentar, Polling. Namun demikian, bangsaonline.com berhak menyunting atau menghapus isi dari pembaca atau pengguna agar tidak merugikan orang lain, lembaga, ataupun badan tertentu, serta menjauhi isi berbau umpatan, pornografi dan sentimen suku, agama dan ras.<br><br> Segala isi baik berupa teks, gambar, video, suara dan segala bentuk grafis yang disampaikan pembaca ataupun pengguna adalah tanggung jawab setiap individu, dan bukan tanggungjawab bangsaonline.com <br><br>Bangsaonline.com ada kalanya menyediakan link ke situs lain, link ini bukan berarti bangsaonline.com menyetujui situs pihak lain. Anda mengetahui dan menyetujui bahwa bangsaonline.com tidak bertanggung jawab atas isi atau materi lainnya yang ada pada situs pihak lain. <br><br>Setiap perjanjian dan transaksi antara anda dan pengiklan yang ada di bangsaonline.com adalah antara anda dan pengiklan. Anda mengetahui dan setuju bahwa bangsaonline.com tidak bertanggung jawab atas segala bentuk kehilangan atau klaim yang mungkin timbul dari perjanjian atau transaksi antara anda dengan pengiklan. <br><br> Semua hasil karya yang dimuat di bangsaonline.com baik berupa teks, gambar, suara dan video serta grafis adalah menjadi hak cipta bangsaonline.com
                        </p>
                    </div>
                    <div class="clear-height-20"></div>


                </div>
            </div>

            <div class="clear"></div>
        </div>
</section>