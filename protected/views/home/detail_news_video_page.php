<section class="detail-news-video-page">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-8">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="#">JATIM</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Travel Health Useful Medical Information For Good Health Be</li>
                    </ol>
                </nav>
                <div class="clear height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="clear-height-25"></div>
                        <div class="title-category">
                            JATIM
                        </div>
                    </div>
                </div>
                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clear-height-5"></div>
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>Travel Health Useful Medical Information For Good Health Be</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="detail-news">
                                <span class="green">Sabtu, 07 April 2018 03:58 WIB</span>
                                <span class="grey"> | Editor : </span>
                                <span class="green"> John Doe</span>
                                <span class="grey"> | Wartawan : </span>
                                <span class="green"> John Doe</span>
                                <button class="btn btn-blue">Bagikan</button>
                            </div>
                        </div>
                    </div>

                    <div class="clear-height-20"></div>

                    <div class="row">
                        <div class="clear-height-20"></div>
                        <div class="col-md-12 video-thumbnail">
                            <img src="<?php echo $this->assetBaseurl; ?>newspage.jpg" class="back img-responsive image">
                            <img src="<?php echo $this->assetBaseurl; ?>button-video.png" class="front">
                        </div>
                    </div>
                    <div class="clear-height-20"></div>

                    <div class="row">
                        <div class="box col-md-12">
                            <div class="box-title">
                                <div class="clear-height-8"></div>
                                <div class="title" style="width : 180px;">
                                    <p> Berita Terkait</p>
                                </div>
                                <div class="clear-height-8"></div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="clear-height-20"></div>
                                            <div class="video-thumbnail">
                                                <img class="image img img-fluid" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg" class="back">
                                                <img src="<?php echo $this->assetBaseurl; ?>button-video.png" class="front2">
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <a href=""><div class="title">
                                                <p>A Discount Toner Cartridge Is Better Then Ever And You Will</p>
                                            </div></a>
                                            <div class="clear-height-5"></div>
                                            <div class="date">01 Oct 2018
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <div class="description">A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC.These handy little</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="clear-height-20"></div>
                                            <div class="video-thumbnail">
                                                <img class="image img img-fluid" src="<?php echo $this->assetBaseurl; ?>content-nasional1.jpg" class="back">
                                                <img src="<?php echo $this->assetBaseurl; ?>button-video.png" class="front2">
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <a href=""><div class="title">
                                                Get The Book A Birds Eye Look Into Mcse Boot Camps
                                            </div></a>
                                            <div class="clear-height-5"></div>
                                            <div class="date">23 Apr 2018
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <div class="description">
                                                You have just invested in a new vehicle, the one of your dreams,and you take it on a trip across Canada. You only put gas in it
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="clear-height-20"> </div>
                                            <div class="video-thumbnail">
                                                <img class="image img img-fluid" src="<?php echo $this->assetBaseurl; ?>content-nasional2.jpg" class="back">
                                                <img src="<?php echo $this->assetBaseurl; ?>button-video.png" class="front2">
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <a href=""><div class="title">
                                                Dvd Replication For Dummies 4 Easy Steps To Professional Dvd
                                            </div></a>
                                            <div class="clear-height-5"></div>
                                            <div class="date">30 Sep 2018
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <div class="description">
                                                The moment you think of buying a Web Hosting Plan, you know one thing - So many choices, which one to choose?
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="clear-height-20"></div>

                            </div>
                        </div>
                    </div>

                    <div class="clear-height-20"></div>

                    <div class="isi-content">
                        <p><b>BANGSAONLINE.com</b> - Conventry is a city with a thoudand years of history that has plenty to offer the visiting tourist. Located in the heart of Warwickshire</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="clear-height-20"></div>
                    <div class="col-md-12">
                        <div class="tags">
                            <div class="clear-height-20"></div>
                            Tags:
                            <button class="btn btn-grey">Berita</button>
                            <button class="btn btn-grey">Berita</button>
                            <div class="clear-height-20"></div>
                        </div>
                    </div>
                </div>

                <div class="clear-height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100"src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php echo $this->renderPartial('//layouts/right-templates-berita-foto', array()); ?>
            </div>
        </div>

       
        <div class="clear"></div>
    </div>
</section>