<section class="homepage">
    <div class="prelative container">
        <div class="content-atas row">
            <div class="col-md-3">
                <a href=""><div class="content">
                    <img src="<?php echo $this->assetBaseurl; ?>content1.jpg">
                    <div class="title">
                        <p>Getting Cheap Airfare For Last Minute Travel</p>
                    </div>
                    <div class="clear-height-5"></div>
                    <div class="description">
                        <p>In the heart of the French Alps, in the north east of the</p>
                    </div>
                </div></a>
            </div>
            <div class="col-md-3">
                <img class="image" src="<?php echo $this->assetBaseurl; ?>270x105.jpg">
            </div>
            <div class="col-md-3">
                <a href=""><div class="content">
                    <img src="<?php echo $this->assetBaseurl; ?>content2.jpg">
                    <div class="title">
                        <p>Getting Cheap Airfare For Last Minute Travel</p>
                    </div>
                    <div class="clear-height-5"></div>
                    <div class="description">
                        <p>In the heart of the French Alps, in the north east of the</p>
                    </div>
                </div></a>
            </div>
            <div class="col-md-3">
                <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>270x105.jpg">
            </div>
        </div>
        <div class="content-utama row">
            <div class="col-md-8">
                <div class="row">
                    <div class="box editorial-box col-md-5">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title">
                                <p>Editorial</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                            <div class="clear-width-20">
                                <a href=""><div class="content">
                                    <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                    <div class="clear-height-15"></div>
                                    <div class="title"><p>Overseas Adventure Travel In Nepal</p></div>
                                    <div class="clear-height-5"></div>
                                    <div class="date"><p>01 Oct 2018</p></div>
                                </div></a>
                                <div class="clear-height-20"></div>
                                <a href=""><div class="content">
                                    <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial2.jpg">
                                    <div class="clear-height-15"></div>
                                    <div class="title"><p>Choose The Perfect Accomodations</p></div>
                                    <div class="clear-height-5"></div>
                                    <div class="date"><p>30 Sep 2018</p></div>
                                </div></a>
                            </div>
                        </div>
                    </div>
                    <div class="box col-md-7">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title" style="width : 200px;">
                                <p>Berita Utama</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                            <div class="clear-height-2">
                                <img class="image" src="<?php echo $this->assetBaseurl; ?>content-berita1.jpg">
                                <div class="clear-height-13"></div>
                            </div>
                            <div class="clear-height-2">
                                <div class="clear-width-20">
                                    <a href=""><div class="title text-center">
                                    <p>Dealing With Technical Support 10 Useful Tips</p>
                                    </div></a>
                                </div>
                            </div>
                            <div class="clear-weight-5">
                                <img src="<?php echo $this->assetBaseurl; ?>content-berita2.jpg">
                                <img src="<?php echo $this->assetBaseurl; ?>content-berita3.jpg">
                                <img src="<?php echo $this->assetBaseurl; ?>content-berita4.jpg">
                                <img src="<?php echo $this->assetBaseurl; ?>content-berita5.jpg">
                                <img src="<?php echo $this->assetBaseurl; ?>content-berita6.jpg">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="clear-height-20"></div>
                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>770x125.jpg">
                        <div class="clear-height-20"></div>
                    </div>
                </div>

                <div class="content-jatim row">
                    <div class="box col-md-12">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title">
                                <p>JATIM</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="clear-width-20">
                                    <div class="col-md-6">
                                        <img class="image" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="content">
                                            <div class="title">
                                                <p>A Discount Toner Cartridge Is Better Then Ever And You Will</p>
                                            </div>
                                            <div class="clear-height-8"></div>
                                            <div class="date"><p>01 Oct 2018</p>
                                            </div>
                                            <div class="clear-height-15"></div>
                                            <div class="description"><p>A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC.These handy little</p></div>
                                            <div class="clear-height-20"></div>
                                        </div></a>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="jatim-small-content">
                                            <a href=""><div class="content-post">
                                                <img src="<?php echo $this->assetBaseurl; ?>post-content1.jpg">
                                                <div class="title">
                                                    <p>7 Ways To Advertise Your Business For Free</p>
                                                </div>
                                                <div class="date">
                                                    <p>08 Apr 2018</p>
                                                </div>
                                            </div></a>
                                            <a href=""><div class="content-post">
                                                <img src="<?php echo $this->assetBaseurl; ?>post-content6.jpg">
                                                <div class="title">
                                                    <p>It S Classified How to Utilize Free Classified Ad Sites To Boost Business</p>
                                                </div>
                                                <div class="date">
                                                    <p>22 May 2018</p>
                                                </div>
                                            </div></a>
                                            <a href=""><div class="content-post">
                                                <img src="<?php echo $this->assetBaseurl; ?>post-content7.jpg">
                                                <div class="title">
                                                    <p>Truck Side Advertising Isn'T It Time</p>
                                                </div>
                                                <div class="date">
                                                    <p>19 Aug 2018</p>
                                                </div>
                                            </div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear-height-20"></div>

                <div class="content-nasional row">
                    <div class="box col-md-12">
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title">
                                <p>NASIONAL</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                            <div class="clear-width-20">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            A Discount Toner Cartridge Is Better Then Ever And You Will
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">01 Oct 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC.These handy little</div>
                                    </div>
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            Get The Book A Birds Eye Look Into Mcse Boot Camps
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">23 Apr 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">
                                            You have just invested in a new vehicle, the one of your dreams,and you take it on a trip across Canada. You only put gas in it</div>
                                    </div>
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-nasional2.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            Dvd Replication For Dummies 4 Easy Steps To Professional Dvd
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">30 Sep 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">
                                            The moment you think of buying a Web Hosting Plan, you know one thing - So many choices, which one to choose?
                                        </div>
                                    </div>
                                </div>
                                <div class="clear-height-20"></div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            A Discount Toner Cartridge Is Better Then Ever And You Will
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">01 Oct 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC.These handy little</div>
                                    </div>
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-editorial1.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            Get The Book A Birds Eye Look Into Mcse Boot Camps
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">23 Apr 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">
                                            You have just invested in a new vehicle, the one of your dreams,and you take it on a trip across Canada. You only put gas in it</div>
                                    </div>
                                    <div class="col-md-4">
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>content-nasional2.jpg">
                                        <div class="clear-height-8"></div>
                                        <a href=""><div class="title">
                                            Dvd Replication For Dummies 4 Easy Steps To Professional Dvd
                                        </div></a>
                                        <div class="clear-height-5"></div>
                                        <div class="date">30 Sep 2018
                                        </div>
                                        <div class="clear-height-8"></div>
                                        <div class="description">
                                            The moment you think of buying a Web Hosting Plan, you know one thing - So many choices, which one to choose?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear-height-20"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                            </div>
                            <div class="col-md-6">
                                <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>

                <div class="content-bawah row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="box col-md-6">
                                <div class="box-title">
                                    <div class="clear-height-8"></div>
                                    <div class="title">
                                        <p>RELIGI</p>
                                    </div>
                                    <div class="clear-height-8"></div>
                                </div>
                                <div class="box-content">
                                    <div class="clear-width-20">
                                     <?php for ($i = 1; $i <= 2; $i++) {?>
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>religicontent1.jpg">
                                        <div class="clear-height-15"></div>
                                        <a href=""><div class="title">Choose The Perfect Accomodations</div></a>
                                        <div class="date">30 Sep 2018</div>
                                        <div class="clear-height-25"></div>
                                     <?php } ?>
                                        <div class="clear-height-8"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="box col-md-6">
                                <div class="box-title">
                                    <div class="clear-height-8"></div>
                                    <div class="title" style="width:180px">
                                        <p>HUKUM & KRIMINAL</p>
                                    </div>
                                    <div class="clear-height-8"></div>
                                </div>
                                <div class="box-content">
                                    <div class="clear-width-20">
                                     <?php for ($i = 1; $i <= 2; $i++) {?>
                                        <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>religicontent1.jpg">
                                        <div class="clear-height-15"></div>
                                        <a href=""><div class="title">Choose The Perfect Accomodations</div></a>
                                        <div class="date">30 Sep 2018</div>
                                        <div class="clear-height-25"></div>
                                     <?php } ?>
                                        <div class="clear-height-8"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-berita-foto row">
                    <div class="box col-md-12">
                        <div class="clear-height-25"></div>
                        <div class="box-title">
                            <div class="clear-height-8"></div>
                            <div class="title " style="width:180px">
                                <p>BERITA FOTO</p>
                            </div>
                            <div class="clear-height-8"></div>
                        </div>
                        <div class="box-content">
                            <div class="clear-width-20">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="clear-weight-6">
                                            <img class="image" src="<?php echo $this->assetBaseurl; ?>foto1.jpg">
                                            <div class="clear-height-13"></div>
                                        </div>
                                        <div class="clear-weight-6">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto2.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto3.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto4.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto5.jpg">
                                            <div class="title">
                                                <div class="clear-height-8"></div>
                                                Family Safari Vacation To The Home Of The Gods
                                            </div>
                                            <div class="date">01 Oct 2018</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="clear-weight-6">
                                            <img class="image" src="<?php echo $this->assetBaseurl; ?>foto1.jpg">
                                            <div class="clear-height-13"></div>
                                        </div>
                                        <div class="clear-weight-6">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto2.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto3.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto4.jpg">
                                            <img src="<?php echo $this->assetBaseurl; ?>foto5.jpg">
                                            <div class="title">
                                                <div class="clear-height-8"></div>
                                                Family Safari Vacation To The Home Of The Gods
                                            </div>
                                            <div class="date">01 Oct 2018</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            <?php echo $this->renderPartial('//layouts/right-templates', array()); ?>

            </div>
        </div>
    </div>
</section>