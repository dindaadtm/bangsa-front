<section class="berita-foto">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-8">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active" aria-current="page">BERITA FOTO</li>
                    </ol>
                </nav>
                <div class="clear height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>/370x125.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>/370x125.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>

                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>BERITA FOTO</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-berita-foto">
                    <div class="row">
                        <?php for ($i = 1; $i <= 3; $i++) {?>
                            <div class="col-md-6">
                                <div class="clear-20">
                                    <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                    <a href=""><div class="title"><p>Overseas Adventure  Travel In Nepal Travel In Nepal</p></div></a>
                                    <div class="date"><p>01 Oct 2018</p></div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php for ($i = 1; $i <= 3; $i++) {?>
                            <div class="col-md-6">
                                <div class="clear-20">
                                    <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                    <a href=""><div class="title"><p>Overseas Adventure  Travel In Nepal</p></div></a>
                                    <div class="date"><p>01 Oct 2018</p></div>
                                </div>
                            </div>
                            <?php } ?>

                    </div>

                </div>

                <div class="col-md-12">
                            <div class="pagination">
                                <a class="active" href="">Sebelumnya</a>
                                <a class="active" href="">1</a>
                                <a href="">2</a>
                                <a href="">3</a>
                                <a class="active" href="">Berikutnya</a>
                            </div>
                        </div>
                <div class="clear-height-20"></div>

                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            <?php echo $this->renderPartial('//layouts/right-templates-no-opini', array()); ?>
            </div>
                <div class="clear"></div>
        </div>
</section>