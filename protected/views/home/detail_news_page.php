<section class="detail-news-page">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-8">
                <nav class="nav-bread" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="#">JATIM</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Travel Health Useful Medical Information For Good Health Be</li>
                    </ol>
                </nav>
                <div class="clear height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="clear-height-25"></div>
                        <div class="title-category">
                            JATIM
                        </div>
                    </div>
                </div>
                <div class="detail-blog-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clear-height-5"></div>
                            <div class="title-news">
                                <img src="<?php echo $this->assetBaseurl; ?>line.jpg" class="line">
                                <p>Travel Health Useful Medical Information For Good Health Be</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="detail-news">
                                <span class="green">Sabtu, 07 April 2018 03:58 WIB</span>
                                <span class="grey"> | Editor : </span>
                                <span class="green"> John Doe</span>
                                <span class="grey"> | Wartawan : </span>
                                <span class="green"> John Doe</span>
                                <button class="btn btn-blue">Bagikan</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="clear-height-20"></div>
                        <div class="col-md-12">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>newspage.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-12">
                            <div class="clear-height-10"></div>
                            <div class="sumber-image">
                                <p>Foto : Pemandangan Alam (Istimewa)</p>
                            </div>
                        </div>
                    </div>
                    <div class="clear-height-20"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box berita-terkait mb-20">
                                <div class="box-title">
                                    <div class="clear-height-8"></div>
                                    <div class="title" style="width : 180px;">
                                        <p>BERITA TERKAIT</p>
                                    </div>
                                    <div class="clear-height-8"></div>
                                </div>
                                <div class="box-content">
                                    <div class="clear-weight-25">
                                        <div class="clear-height-25"></div>
                                        <a href=""><div class="title"><p>Overseas Adventure Travel In Nepal</p></div></a>
                                        <div class="date"><p>01 Oct 2018</p></div>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="clear-weight-25">
                                        <div class="clear-height-25"></div>
                                        <div class="clear-height-25"></div>
                                        <a href=""><div class="title"><p>Choose The Perfect Accomodations</p></div></>
                                        <div class="date"><p>30 Sep 2018</p></div>
                                        <div class="clear-height-8"></div>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="clear-weight-25">
                                        <div class="clear-height-25"></div>
                                        <div class="clear-height-25"></div>
                                        <a href=""><div class="title"><p>Choose The Perfect Accomodations</p></div></a>
                                        <div class="date"><p>30 Sep 2018</p></div>
                                        <div class="clear-height-8"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="isi-content">
                                <p><b>BANGSAONLINE.com</b> - Conventry is a city with a thoudand years of history that has plenty to offer the visiting tourist. Located in the heart of Warwickshire</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat ea nihil in quos libero eos cumque mollitia dolorum ipsa necessitatibus eveniet, magni rerum aperiam temporibus qui harum consequatur voluptates laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium natus tempore explicabo quaerat libero, a consectetur id iure amet quasi optio iusto iste ab mollitia officia necessitatibus quis autem perferendis.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="clear-height-20"></div>
                        <div class="col-md-12">
                            <div class="tags">
                                <div class="clear-height-20"></div>
                                Tags:
                                <button class="btn btn-grey">Berita</button>
                                <button class="btn btn-grey">Berita</button>
                                <div class="clear-height-20"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="clear-height-30">
                            <div class="row">
                                <a href=""><div class="box col-md-6">
                                    <div class="box-title">
                                        <div class="clear-height-8"></div>
                                        <div class="title" style="width : 200px;">
                                            <p>BERITA SEBELUMNYA</p>
                                        </div>
                                        <div class="clear-height-8"></div>
                                    </div>
                                    <div class="box-content">
                                        <div class="clear-width-20">
                                            <img src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                            <div class="clear-height-25"></div>
                                            <div class="title"><p>Overseas Adventure Travel In Nepal</p></div>
                                            <div class="date"><p>01 Oct 2018</p></div>
                                            <div class="clear-height-25"></div>
                                        </div>
                                    </div>
                                </div></a>
                                <a href=""><div class="box col-md-6">
                                    <div class="box-title">
                                        <div class="clear-height-8"></div>
                                        <div class="title" style="width : 200px;">
                                            <p>BERITA SELANJUTNYA</p>
                                        </div>
                                        <div class="clear-height-8"></div>
                                    </div>
                                    <div class="box-content">
                                        <div class="clear-width-20">
                                            <img src="<?php echo $this->assetBaseurl; ?>jatimcontent1.jpg" class="img-responsive">
                                            <div class="clear-height-25"></div>
                                            <div class="title"><p>Overseas Adventure Travel In Nepal</p></div>
                                            <div class="date"><p>01 Oct 2018</p></div>
                                            <div class="clear-height-25"></div>
                                        </div>
                                    </div>
                                </div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear-height-20"></div>
                <div class="ads">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100" src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                        <div class="col-md-6">
                            <img class="image img img-fluid w-100"src="<?php echo $this->assetBaseurl; ?>370x125.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php echo $this->renderPartial('//layouts/right-templates-berita-foto', array()); ?>
            </div>
        </div>

       
        <div class="clear"></div>
    </div>
</section>