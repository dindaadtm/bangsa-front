<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	// public function actionDummy()
	// {
	// 	// Dummy::createDummyProduct();
	// 	// echo '<META http-equiv="refresh" content="0;URL=http://localhost/dv-computers/home/dummy">';
		
	// 	$data = Data::model()->findAll('1 LIMIT '.$_GET['page'].', 30');
	// 	foreach ($data as $key => $value) {
	// 		// 106-large_default.jpg
	// 		$image_name = '';
	// 		if ($value->image != 'http://ufoelektronika.com/img/p/0.jpg') {
	// 			$url = $value->image;
	// 			$url = substr($url, 0, -4).'-large_default.jpg';
	// 			$image_name = Slug::create($value->id.'-'.$value->name).'.jpg';
	// 			$img = Yii::getPathOfAlias('webroot').'/images/product/'.$image_name;
	// 			@file_put_contents($img, file_get_contents($url));
	// 		}

	// 		$model = new PrdProduct;
	// 		$model->kode = $value->reference;
	// 		$model->image = $image_name;
	// 		$model->harga = $value->base_price;
	// 		$model->harga_coret = $value->base_price + ($value->base_price*20/100);
	// 		$model->stock = $value->qty;
	// 		$model->status = $value->status;
	// 		$model->save(false);

	// 		$modelDesc = new PrdProductDescription;
	// 		$modelDesc->product_id = $model->id;
	// 		$modelDesc->language_id = 2;
	// 		$modelDesc->name = $value->name;
	// 		$modelDesc->save(false);
	// 	}
	// 	echo '<a href="'.CHtml::normalizeUrl(array('dummy', 'page'=>$_GET['page']+30)).'">next</a>';

	// }

	public function actionCreatecategory()
	{
		for ($i=1; $i < 111; $i++) { 
			$model = new PrdCategoryProduct;
			$model->category_id = 12;
			$model->product_id = $i;
			$model->save(false);
		}
	}

	public function actionInput()
	{
		$data = Table61::model()->findAll();
		foreach ($data as $key => $value) {
			$model = new PrdProduct;
			$model->kode = $value->col_1;
			if ($value->col_7 != '') {
				copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/COVER.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg');
				$model->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg';
			}else{
				$model->image = '';
			}
			$model->harga = $value->col_6;
			$model->harga_coret = 0;
			$model->save(false);
			$dataDesc = new PrdProductDescription;
			$dataDesc->product_id = $model->id;
			$dataDesc->language_id = 2;
			$dataDesc->name = $value->col_2;
			$dataDesc->subtitle = $value->col_3;
			$dataDesc->desc = '<p>'.$value->col_4.'</p>';
			$dataDesc->save(false);
			$dataAttr = explode(',', $value->col_5);
			foreach ($dataAttr as $v) {
				$modelAttr = new PrdProductAttributes;
				$modelAttr->product_id = $model->id;
				$modelAttr->attribute = trim($v);
				$modelAttr->stock = 10;
				$modelAttr->price = $value->col_6;
				$modelAttr->save(false);
				$modelAttr->id_str = $modelAttr->id;
				$modelAttr->save(false);

			}
			if ($value->col_7 != '') {
				for ($i=1; $i < 7; $i++) { 
					$modelImage = new PrdProductImage;
					$modelImage->product_id = $model->id;
					copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/'.$i.'.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg');
					$modelImage->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg';
					$modelImage->save(false);
				}
			}



		}
	}

	public function actionIndex()
	{
		// $criteria2=new CDbCriteria;
		// $criteria2->with = array('description');
		// $criteria2->order = 'date DESC';
		// $criteria2->addCondition('status = "1"');
		// $criteria2->addCondition('description.language_id = :language_id');
		// $criteria2->params[':language_id'] = $this->languageID;

		// if ($_GET['category']) {
		// 	$criteria = new CDbCriteria;
		// 	$criteria->with = array('description');
		// 	$criteria->addCondition('t.id = :id');
		// 	$criteria->params[':id'] = $_GET['category'];
		// 	$criteria->addCondition('t.type = :type');
		// 	$criteria->params[':type'] = 'category';
		// 	$criteria->order = 'sort ASC';
		// 	$strCategory = PrdCategory::model()->find($criteria);

		// 	// $inArray = PrdProduct::getInArrayCategory($_GET['category']);
		// 	// $criteria2->addInCondition('t.category_id', $inArray);
		// 	$criteria2->addCondition('t.tag LIKE :category');
		// 	$criteria2->params[':category'] = '%category='.$_GET['category'].',%';
		// }else{
		// 	$criteria2->addCondition('t.tag LIKE :category');
		// 	$criteria2->params[':category'] = '%category=35,%';
		// }
		// $pageSize = 8;

		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria2,
		//     'pagination'=>array(
		//         'pageSize'=>$pageSize,
		//     ),
		// ));

		// $model = new ContactForm;
		// $model->scenario = 'insert';

		$this->layout='//layouts/column2';
		$this->render('index', array(
			// 'product'=>$product,
			// 'model'=>$model,
		));
	}

	public function actionDealer()
	{
		$this->pageTitle = 'Dealer Locator - '.$this->pageTitle;

		$this->layout='//layouts/column2';
		$criteria = new CDbCriteria;
		if ($_GET['kota'] != '') {
			$criteria->addCondition('kota = :kota');
			$criteria->params[':kota'] = $_GET['kota'];
		}
		if ($_GET['prov'] != '') {
			$criteria->addCondition('prov = :prov');
			$criteria->params[':prov'] = $_GET['prov'];
		}
		if ($_GET['loc'] == 'dealer-location') {
			$criteria->addCondition('type = :type');
			$criteria->params[':type'] = 'dealer';
		}elseif($_GET['loc'] == 'asp-location'){
			$criteria->addCondition('type = :type');
			$criteria->params[':type'] = 'asp';
		}
		$criteria->order = 'sort ASC';
		$dataAddress2 = array();
		// if ($_GET['kota'] != '') {
			$dataAddress2 = Address::model()->findAll($criteria);
			// foreach ($dataAddress as $key => $value) {
			// 	$dataAddress2[$value->kota][] = $value;
			// }
		// }

		$criteria2 = new CDbCriteria;
		if ($_GET['loc'] == 'dealer-location') {
			$criteria2->addCondition('type = :type');
			$criteria2->params[':type'] = 'dealer';
		}elseif($_GET['loc'] == 'asp-location'){
			$criteria2->addCondition('type = :type');
			$criteria2->params[':type'] = 'asp';
		}
		$criteria2->group = 'prov';
		$listProv = Address::model()->findAll($criteria2);

		$listKota = array();
		if ($_GET['prov'] != '') {
			$criteria = new CDbCriteria;
			if ($_GET['loc'] == 'dealer-location') {
				$criteria->addCondition('type = :type');
				$criteria->params[':type'] = 'dealer';
			}elseif($_GET['loc'] == 'asp-location'){
				$criteria->addCondition('type = :type');
				$criteria->params[':type'] = 'asp';
			}
			$criteria->addCondition('prov = :prov');
			$criteria->params[':prov'] = $_GET['prov'];
			$criteria->group = 'kota';
			$listKota = Address::model()->findAll($criteria);
		}


		$this->render('dealer', array(
			'dataAddress'=>$dataAddress2,
			'listKota'=>$listKota,
			'listProv'=>$listProv,
		));
	}

	public function actionFaq()
	{
		$this->layout='//layouts/column2';

		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->order = 't.id ASC';

		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$pageSize = 100000;

		$dataFaq = new CActiveDataProvider('Faq', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->render('faq', array(
			'dataFaq'=>$dataFaq,
		));
	}


	public function actionPcontact()
	{
		$model = new ContactForm;
		$model->scenario = 'insert';
		
		if(isset($_POST['ContactForm']))
		{
			if (!isset($_POST['g-recaptcha-response'])) {
				$this->redirect(array('home/career'));
	        }
	        

				$model->attributes=$_POST['ContactForm'];
				if($model->validate())
				{
			        $secret_key = "6Lc5ExQUAAAAAHgV4U_6krEDyf-ykhlx08mEgJek";
			        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
			        $response = json_decode($response);
			        if($response->success==false)
			        {
			          $model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
			        }else{
						// config email
						$messaged = $this->renderPartial('//mail/contact2',array(
							'model'=>$model,
						),TRUE);
						$config = array(
							'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
							'subject'=>'[UFO Elektronika] Contact from '.$model->email,
							'message'=>$messaged,
						);
						if ($this->setting['contact_cc']) {
							$config['cc'] = array($this->setting['contact_cc']);
						}
						if ($this->setting['contact_bcc']) {
							$config['bcc'] = array($this->setting['contact_bcc']);
						}
						// kirim email
						Common::mail($config);

						Yii::app()->user->setFlash('success','Trimakasih telah mengirimkan pesan kepada kami, kami akan segera membalas pesan anda');
						$this->refresh();
					}
				}


		}

		$this->layout='//layouts/column2';
		$this->pageTitle = 'Contact Us - '.$this->pageTitle;
		$this->render('pcontact', array(
			'model'	=>$model,
		));
	}

	public function actionSugest()
	{

		if ($_POST['q'] != '') {
			$str = '<p id="searchresults">';
	            $criteria=New CDbCriteria ; 
	            $criteria->addCondition('(name LIKE :q OR tag LIKE :q)');
	            $criteria->params[':q'] = '%'.$_POST['q'].'%';
	            $criteria->addCondition('language_id = :language_id');
	            $criteria->params[':language_id'] = $this->languageID;

	            $criteria->order = 'date_input DESC';
	            $criteria->limit = 5;
	            $list = ViewProduct::model()->findAll($criteria);
	            
				$str .= '<span class="category">Search: '.$_POST['q'].'</span>';
	            foreach($list as $value)
	            {
					$str .= '<a href="'.CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)).'">
						<span class="searchheading">'.$value->name.'</span>
					</a>';
				}
			// }
			$str .= '<span class="seperator"><a href="'.CHtml::normalizeUrl(array('/product/index', 'q'=>$_POST['q'])).'" title="Sitemap">See other result for '.$_POST['q'].' &gt;</a></span>
			<br class="break">
			</p>
			';
			echo $str;
		}
	}

	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{

				$criteria=new CDbCriteria;
				$criteria->with = array('description', 'category', 'categories');
				$criteria->order = 'date DESC';
				$criteria->addCondition('status = "1"');
				$criteria->addCondition('terlaris = "1"');
				$criteria->addCondition('description.language_id = :language_id');
				$criteria->params[':language_id'] = $this->languageID;
				$pageSize = 12;
				$criteria->group = 't.id';
				$product = new CActiveDataProvider('PrdProduct', array(
					'criteria'=>$criteria,
				    'pagination'=>array(
				        'pageSize'=>$pageSize,
				    ),
				));


				$criteria = new CDbCriteria;
				$criteria->with = array('description');
				$criteria->addCondition('t.parent_id = :parent_id');
				$criteria->params[':parent_id'] = 0;
				$criteria->addCondition('t.type = :type');
				$criteria->params[':type'] = 'category';
				$criteria->limit = 3;
				$criteria->order = 'sort ASC';
				$categories = PrdCategory::model()->findAll($criteria);

				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
					'product'=>$product,
					'categories'=>$categories,
				));
			}
		}

	}


	public function actionDetail_berita_foto()
	{
		$this->pageTitle = 'Detail berita foto - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('detail_berita_foto', array(	
		));
	}
	public function actionDetail_news_page()
	{
		$this->pageTitle = 'Detail news page - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('detail_news_page', array(	
		));
	}

	public function actionDetail_news_video_page()
	{
		$this->pageTitle = 'Detail news video page - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('detail_news_video_page', array(	
		));
	}


	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionKanal_kategori()
	{
		$this->pageTitle = 'Kategori - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('kanal_kategori', array(	
		));
	}
	
	public function actionIndeks_berita()
	{
		$this->pageTitle = 'indeks berita - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('indeks_berita', array(	
		));
	}

	public function actionBerita_foto()
	{
		$this->pageTitle = 'berita foto - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('berita_foto', array(	
		));
	}
	public function actionSearch()
	{
		$this->pageTitle = 'Search - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('search', array(	
		));
	}

	public function actionDisclaimer()
	{
		$this->pageTitle = 'disclaimer - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('disclaimer', array(	
		));
	}
	public function actionKontak()
	{
		$this->pageTitle = 'kontak - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('kontak', array(	
		));
	}
	public function actionPedoman()
	{
		$this->pageTitle = 'pedoman - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('pedoman', array(	
		));
	}
}


